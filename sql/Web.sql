﻿CREATE DATABASE `web`;
USE `web`;


CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `title` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `home` (`id`, `title`, `text`) VALUES
(1, 'Welcome to bcs.net.nz', '                                                                                                                                                                                                                                                            Toi-Ohomai Institute of Technology & University of Waikato\r\n                Bachelor of Computing and Mathematical Science,This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.\r\n                 Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree                                                                                                                                                                                                                                ');

CREATE TABLE `information` (
    `id` int AUTO_INCREMENT,
    `title` text,
    `text` text,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `information` (`id`, `title`, `text`) VALUES
(1, 'About the BSC Network', '                                                                        The BCS network or known as Pandora Network is completely seperate from \r\n                        the corporate network meaning:\r\n                        Your H: Drives in the corporate network are different to pandora labs\r\n                       None of your resourse can be shared                                                                 '),
(2, 'Moodle Logon', '                                                                                                              Your Login for moodle is your corporate login and this is the same for when \r\n                        you log onto one of the computers in the corporate labs.\r\n                        Your login for BCS labs (Pandora) is your Student ID and the password you chose.                                                                                                '),
(3, 'Wifi Logon', '                                                                                                                                                To connect your device to the Student Wifi, Select Toi Ohomai Wi-Fi in your Wifi Settings,\r\n                      Your Username = moodle-id@stu.boppoly.ac.nz. Yous Password = your student id (unless you have changed it)                                                                                                                                '),
(4, 'Outlook', 'Your email address for you outlook email account will be your corporate\r\n                        username as well as the address "corporate-username@stu.boppoly.ac.nz"'),
(5, 'Google Apps login', ' You wil login with your BSC (Pandora) credentials.\r\n                            your email will be "student-id@bcs.net.nz"'),
(6, 'Office Login', 'To get Office 365 at home you will need to sign in with your corporate \r\n                        credentials.'),
(7, 'Microsoft Imagine Login', 'For imagine login you will also use your BCS (Pandora) credentials "student-id@bcs.net.nz"\r\n                        and your chosen password. You can download the software and keep it for free.'),
(8, 'Google Chrome Sign In', 'To sign into google chrome, at the top right hand corner there will be a grey box.\r\n                        Click on this box and add youself in there by signing in.\r\n                        Use your BCS (Pandora) email "student-id@bcs.net.nz" and the password you create.\r\n                        Once you do this Link the data and you have access to things like the bcs bookmarks etc.');

CREATE TABLE `navbar` (
    `id` int AUTO_INCREMENT,
    `Home` varchar(25),
    `quicklinks` varchar(25),
    `information` varchar(25),
    `pathways` varchar(25),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `pathways` (
    `id` int AUTO_INCREMENT,
    `title` text,
    `text` text,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pathways` (`id`, `title`, `text`) VALUES
(1, 'Certificate in Computing (CIC) Level 2', '                                                                        This course is designed to improve your digital literacy. You may be returning to the workforce or simply wanting to keep up with technology and meet and learn with others. You will use a range of software tools and applications that are freely available online to create and share digital content including images, audio and video. You will learn how to connect and collaborate with others online, and will have the opportunity to use a range of digital devices including PCs, phones and tablets. You will learn about basic troubleshooting and personal cyber-safety.                                                                 '),
(2, 'New Zealand Certificate in Information Technology Essentials (Level 4)', '                                                                        This one semester computing course gives you a basic knowledge of computer hardware, operating systems, applications and networks so you can get your IT career off to a strong start. During your study you''ll learn about the main components of a computer and explain the way in which they interact, assembly, disassembly and maintenance, troubleshooting common problems, operating systems and end user security, and internet-based services. You will also complete an introduction to computational thinking and basic programming.                                                                 '),
(3, 'Diploma in Applied Computing (Level 5)', 'Want to get a job as a computer programmer, systems analyst, website or software developer? Technology is rapidly changing the way we live and New Zealand desperately needs more computer professionals. Level 5 and Level 6 Diplomas offer the only local pathway towards the University of Waikatoâ€™s Bachelor of Science, specialising in Applied Computing. This Level 5 Diploma in Applied Computing develops core skills in IT infrastructure, software, multimedia, networks and programming to meet a wide range of business computing needs. '),
(4, 'Diploma in Applied Computing (Level 6)', 'This computing and IT course develops your career as an intermediate IT professional. The Level 6 Diploma includes website development, business and project management, database systems and advanced programming. Complete just one more year after this diploma to gain the University of Waikatoâ€™s Bachelor of Science (BSc) majoring in Computer Science with a specialisation in Applied Computing, taught right here in Tauranga. Students benefit from strong industry connections and get plenty of opportunities to put theory into practice, using their creative and analytical skills to develop portfolios of their work to present to future employers '),
(5, 'Bachelor of Science (BSc) in Computer Science with Specialisation in Applied Computing', 'There is no need to leave Tauranga to get your computing degree. Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree, delivered in Tauranga. You''ll take courses on software systems, web application and development, how people and computers interact and how to ensure systems work well and are easy to use. Throughout the degree you''ll continuously put into practice what you learn through hands-on, practical projects. ');

CREATE TABLE `quicklinks` (
    `id`int AUTO_INCREMENT,
    `title` text,
    `image`text,
    `text` text,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `quicklinks` (`id`, `title`, `image`, `text`) VALUES
(1, 'Moodle', '../images/moodle.png', '                                                                                                            Moodle is where you as students will logon to find all of your course\r\n                        information you will need such as class times, assessment dates and\r\n                        Class information. Need help.\r\n                        Go to the info page.                                                                                                '),
(2, 'TOI-OHOMAI', '../images/logo.png', '                                                                                                            This is the main Polytechnic website where you will find all of \r\n                        the inforamtion about every course which includes course fees,\r\n                        start/finish dates, what topics you will be studying in your chosen\r\n                        course etc.                                                                                                '),
(3, 'Outlook', '../images/outlook.png', '                                    This is your personal polytechnic email address. You can also use your\r\n                        own email address if you wish. For help to log onto your email address\r\n                        go to the information page.                                '),
(4, 'Microsoft Imagine', '../images/imagine.png', 'Microsoft Imagine previously known as Microsoft Dreamspark is where you as \r\n                            students are able to download software such as Visual Studio for free.\r\n                            To learn how to log into Microsoft Imagine go to the information page.'),
(5, 'Office 365', '../images/office.png', 'Get 5 Office licenses for free while you study.'),
(6, 'Visual Studio', '../images/visual.png', 'Clicking on this link will start an automatic download of visual Studio\r\n                    community version. You will use this a lot in this course.'),
(7, 'Bitbucket', '../images/bitbucket.png', 'Bitbucket is a web-based hosting service you will use when you are creating\r\n                    projects such as websites. You can make your own account with your private email\r\n                    but for in class use your tutor may get you to make a bcs email.'),
(8, 'Google Drive', '../images/google.png', 'Google drive is where you can upload and save your work and be able to access it\r\n                    anywhere. If you log in with your BSC credentials you will have unlimited storage.\r\n                    to find out what your credentials are go to the information page.');

CREATE TABLE `user` (
    `id` int AUTO_INCREMENT,
    `username` varchar (25),
    `password` varchar (25),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'AmyM', 'password');
